package com.vov4ik.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyLogger {
    private static Logger logger = LogManager.getLogger(MyLogger.class);

    public void printDebug(String message) {
        logger.debug(message);
    }
    public void printInfo(String message) {
        logger.info(message);
    }
    public void printWarning(String message) {
        logger.warn(message);
    }
    public void printError(String message) {
        logger.error(message);
    }
    public void printFatal(String message) {
        logger.fatal(message);
    }







}
