package com.vov4ik.game;

import com.vov4ik.game.npc.Artifact;
import com.vov4ik.game.npc.Monster;
import com.vov4ik.game.npc.NPC;

import java.util.Random;

public class RoundHall {
    private final int numberOfDoors = 10;
    private Hero hero;
    private NPC[] npcArray;

    public RoundHall() {
        npcArray = new NPC[numberOfDoors];
        hero = new Hero("Terminator");
        generateHall();
    }

    private void generateHall() {
        Random random = new Random();
        for (int i = 0; i < numberOfDoors; i++) {
            if (random.nextBoolean()) {
                npcArray[i] = new Monster();
            } else {
                npcArray[i] = new Artifact();
            }
        }
    }

    public void openAllDoors() {
        for (NPC npc : npcArray) {
            System.out.println(npc);
        }
    }
}
