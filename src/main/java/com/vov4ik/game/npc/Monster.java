package com.vov4ik.game.npc;

public class Monster extends NPC {
    final static int minPower = 5;
    final static int maxPower = 100;
    final static NPCType npcType = NPCType.Monster;

    public Monster() {
        super(minPower, maxPower, npcType);
    }
}