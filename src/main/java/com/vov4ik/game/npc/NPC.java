package com.vov4ik.game.npc;

import java.util.Random;

public abstract class NPC  {
    private final int power;
    private final NPCType npcType;

    public NPC(int minPower, int maxPower, NPCType npcType) {
        Random random = new Random();
        this.npcType = npcType;
        this.power = random.nextInt(maxPower - minPower) + minPower;
    }

    public int getPower() {
        return power;
    }

    public NPCType getNpcType() {
        return npcType;
    }

    @Override
    public String toString() {
        return npcType.name() +
                " power = " + power;
    }
}
