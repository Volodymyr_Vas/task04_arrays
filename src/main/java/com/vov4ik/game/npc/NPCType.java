package com.vov4ik.game.npc;

public enum NPCType {
    Artifact,
    Monster
}
