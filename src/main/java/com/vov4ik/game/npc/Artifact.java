package com.vov4ik.game.npc;

public class Artifact extends NPC {
    final static int minPower = 10;
    final static int maxPower = 80;
    final static NPCType npcType = NPCType.Artifact;

    public Artifact() {
        super(minPower, maxPower, npcType);
    }
}

