package com.vov4ik.arrays;

import com.vov4ik.logger.MyLogger;

import java.util.Arrays;

public class ThirdArrayFromTwo {
    public static void main(String[] args) {
        MyLogger myLogger = new MyLogger();
        int [] firstArray = new int[10];
        int [] secondArray = new int[15];
        ArrayChanger arrayChanger = new ArrayChanger();
        arrayChanger.fillArray(firstArray);
        arrayChanger.fillArray(secondArray);
        int[] repeatedArray = arrayChanger.makeArrayOfRepeatedElements(firstArray, secondArray);
        myLogger.printInfo("This is array with copies: ");
        myLogger.printInfo(Arrays.toString(repeatedArray));
        int[] notRepeatedArray = arrayChanger.makeArrayOfDifferentElements(firstArray, secondArray);
        myLogger.printInfo("Array with not copied elements: ");
        myLogger.printInfo(Arrays.toString(notRepeatedArray));
        int[] lessThenTwoCopiesArray = arrayChanger.removeCopiesFromArray(secondArray);
        myLogger.printInfo("Array with less then 2 copies elements: ");
        myLogger.printInfo(Arrays.toString(lessThenTwoCopiesArray));
        int[] withoutSeriesOfCopiesArray = arrayChanger.removeSeriesCopiesFromArray(secondArray);
        myLogger.printInfo("Array without series of copies elements: ");
        myLogger.printInfo(Arrays.toString(withoutSeriesOfCopiesArray));
    }
}
