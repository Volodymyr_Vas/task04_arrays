package com.vov4ik.arrays;

import com.vov4ik.logger.MyLogger;

import java.util.Arrays;
import java.util.Random;

public class ArrayChanger {
    private MyLogger logger = new MyLogger();

    void fillArray(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(3);
        }
        logger.printInfo(Arrays.toString(array));
    }

    private boolean contains(int[] array, int element) {
        boolean contains = false;
        for (int value : array) {
            if (value == element) {
                contains = true;
            }
        }
        return contains;
    }

    int[] makeArrayOfRepeatedElements(int[] firstArray, int[] secondArray) {
        int counter = 0;
        int[] tmpArray = new int[firstArray.length];
        for (int value : firstArray) {
            if (contains(secondArray, value)) {
                tmpArray[counter] = value;
                counter++;
            }
        }
        int[] arrayWithCopies = new int[counter];
        System.arraycopy(tmpArray, 0, arrayWithCopies, 0, counter);
        return arrayWithCopies;
    }

    int[] makeArrayOfDifferentElements(int[] firstArray, int[] secondArray) {
        int counter = 0;
        int[] tmpArray = new int[firstArray.length + secondArray.length];
        for (int value : firstArray) {
            if (!contains(secondArray, value)) {
                tmpArray[counter] = value;
                counter++;
            }
        }
        for (int value : secondArray) {
            if (!contains(firstArray, value) && !contains(tmpArray, value)) {
                tmpArray[counter] = value;
                counter++;
            }
        }
        int[] arrayWithoutCopies = new int[counter];
        System.arraycopy(tmpArray, 0, arrayWithoutCopies, 0, counter);
        return arrayWithoutCopies;
    }

    int[] removeCopiesFromArray(int[] inputArray) {
        int copiesCounter;
        int newElementsCounter = 0;
        int searchedElement;
        int index = 0;
        int[] tmpArray = new int[inputArray.length];
        for (int element : inputArray) {
            searchedElement = element;
            copiesCounter = 0;
            for (int value : inputArray) {
                if (searchedElement == value) {
                    copiesCounter++;
                }
            }
            if (copiesCounter <= 2) {
                tmpArray[index++] = searchedElement;
                newElementsCounter++;
            }
        }
        int[] removedCopiesArray = new int[newElementsCounter];
        System.arraycopy(tmpArray, 0, removedCopiesArray, 0, newElementsCounter);
        return removedCopiesArray;
    }

    int[] removeSeriesCopiesFromArray(int[] inputArray) {
        int newElementsCounter = 1;
        int searchedElement;
        int index = 0;
        int[] tmpArray = new int[inputArray.length];
        tmpArray[index++] = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            searchedElement = inputArray[i - 1];
            if (searchedElement != inputArray[i]) {
                tmpArray[index++] = inputArray[i];
                newElementsCounter++;
            }
        }
        int[] removedSeriesCopiesArray = new int[newElementsCounter];
        System.arraycopy(tmpArray, 0, removedSeriesCopiesArray, 0, newElementsCounter);
        return removedSeriesCopiesArray;
    }
}
